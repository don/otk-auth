const bcrypt = require('bcrypt');
const { normalize } = require('../utils');

async function handleLogin(account, password, context) {
  const { knex: db, jwt, log, user, auth } = context.app; 

  if (auth) {
    return user;
  } else {
    // Check to see if it's a username or an e-mail address  
    const payload = (/@/.test(account)) ? { email: account } : { username: account };

    const userResults = await db('users').where(payload).limit(1);

    if (userResults && userResults.length) {
      const user = userResults[0];

      if(bcrypt.compareSync(password, user.password_digest)) {
        // Valid user    
        user.jwt = jwt.sign({id: user.id, admin: user.admin});
        return user;
      } else {
        return new Error("Invalid credentials");
      }
    } else {
      return new Error("Invalid credentials");
    }
  }
}

async function handleSignup(args, context) {
  const { username, email, password } = args;
  const { knex: db, jwt, log, user, auth } = context.app; 

    if (auth) {
      log.info(`User ${user.id} tried to signup while logged in.`);
      return user;
    } else {
      //No @'s in usernames
      if (/@/.test(username)) {
        return new Error("Username cannot contain @ symbol");
      }

      if (username.length && email.length) {
        // First, check to see if email exists in db
        const exists = Boolean((await db('users').where('email', email).limit(1)).length);

        if (exists) {
          log.error("Tried to sign up with duplicate e-mail.", email);
          return new Error("Email already exists.");
        }

        const password_digest = bcrypt.hashSync(password, 10);
        const [ nUsername, nEmail ] = normalize([username, email]);
        const newUser = {
          username: nUsername, 
          email: nEmail,
          password_digest
        };
        const user = await db('users').insert(newUser).returning('*');
        user.jwt = jwt.sign({id: user.id, admin: user.admin});
        return user;
      } else {
        log.error("Tried to sign up with 0-length username and/or email");
        return new Error("Please provide a username and e-mail address.");
      }
    }
}

module.exports = {
  handleLogin,
  handleSignup
};
