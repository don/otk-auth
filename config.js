process.env.NODE_ENV = process.env.NODE_ENV || 'development';

//.env.development, .env.production
require('dotenv').config({path: `.env.${process.env.NODE_ENV}`});
const convict = require('convict');

const config = convict({
  env: {
    doc: "The Node environment",
    format: String,
    default: "development",
    env: 'NODE_ENV'
  },
  jwtSecret: {
    doc: "Secret for encoding auth JWTs",
    format: String,
    default: "pfudor",
    env: "JWT_SECRET"
  },
  port: {
    doc: "The port the engine runs on",
    format: Number,
    default: 3000,
    env: "PORT"
  },
  db: {
    port: {
      doc: "Port on which db is running",
      format: Number,
      default: 5432,
      env: "DB_PORT"
    },
    host: {
      doc: "Host on which db is running",
      format: String,
      default: "127.0.0.1",
      env: "DB_HOST"
    },
    user: {
      doc: "Username for the db",
      format: String,
      default: '',
      env: "DB_USER"
    },
    password: {
      doc: "Password for the db",
      format: String,
      default: '',
      env: "DB_PASS"
    },
    database: {
      doc: "Db name",
      format: String,
      default: 'forfans',
      env: "DB_NAME"
    }
  }
});

config.validate({allowed: 'strict'});

module.exports = config;
