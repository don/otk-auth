const { queries, mutations }  = require('./main');
const userResolvers           = require('./user');

module.exports = { 
  Query: queries, 
  Mutation: mutations, 
  User: userResolvers
};

