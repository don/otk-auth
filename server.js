// Require the framework and instantiate it
const fastify = require('fastify')({ logger: true });
const gql = require('fastify-gql');
const helmet  = require('fastify-helmet');
const config = require('./config');
const knexConfig = require('./knexfile');

const schema  = require('./data/schema');
const resolvers = require('./data/resolvers');

fastify.register(helmet);
fastify.register(require('fastify-sensible'));
fastify.register(require('fastify-helmet'));
fastify.register(require('fastify-formbody'));
fastify.register(require('fastify-knexjs'), knexConfig, err => console.error(err));
fastify.register(require('fastify-jwt'), {
  secret: config.get('jwtSecret')
});

fastify.register(gql, { schema });

// Declare a route
fastify.route({
  method: 'POST',
  url: '/gql',
  preHandler: (req, reply, done) => {
    if (req.headers.authorization) {
      const token = fastify.jwt.decode(req.headers.authorization.split(' ')[1]);

      if (token) {
        //select * from users where id='id' limit 1;
        fastify.knex('users').where('id', +token.id).limit(1).then(users => {
          req.user = users[0] || null;
          done();
        })
        .catch(err => {
          req.log.error({message: err.message, stack: err.stack, severity: 'Error'}, 'Error in loading user from JWT');
          done(err);
        });
      } else {
        req.user = null;
        done();
      }
    } else {
      req.user = null;
      done();
    }
  },
  handler: async (request, reply) => {
    const query = request.body.query;
    if (process.env.NODE_ENV === 'development') {
      request.log.info({msg: 'Got a request', payload: query});
    }

    // Second param is gql context
    return fastify.graphql(query, { user: request.user, auth: Boolean(request.user) });
  }
});

fastify.get('/', async (request, reply) => {
  return {get: {
    the: {
      fuck: 'out'
    }
  };
});

// Run the server!
const start = async () => {
  try {
    await fastify.listen(3000);
    fastify.log.info(`server listening on ${fastify.server.address().port}`);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
