const config = require('./config')

module.exports = {
  client: 'postgresql',
  connection: config.get('db'),
  pool: {
    min: 2,
    max: 10
  },
  migrations: {
    directory: './db/migrations',
    tableName: 'knex_migrations'
  }
};
