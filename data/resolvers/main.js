const { handleLogin, handleSignup } = require('./handleAccounts');

const queries = {
  user: async (root, args, context) => {
    return (await knex('users').where('id', args.userId).limit(1))[0];  
  }
};

const mutations = {
  login: async (root, { account, password }, context) => {
    return await handleLogin(account, password, context.app);
  },
  signup: async function (root, args, context) {
    return await handleSignup(args, context.app);
  }
};

module.exports = { queries, mutations };
