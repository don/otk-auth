exports.up = function(knex, Promise) {
  return knex.schema.createTable('users', t => {
    t.increments();
    t.string('username');
    t.string('email').unique();
    t.string('password_digest');
    t.boolean('admin').index().defaultTo(false);
    t.timestamps(true, true);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('users'); 
};
