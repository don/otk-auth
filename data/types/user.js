const typeDef = `
type User {
  id: ID!
  jwt: String
  username: String
  email: String
  admin: Boolean
  rando: Int
}
`;

module.exports = typeDef;
